#include <iostream>
#include <string>
#include <cstdlib>
#include <string.h>
#include <fstream>
#include <stdio.h>
#include <vector>
#include <iomanip>
#include <algorithm>
#include <cstring>

using namespace std;

int main(int argc, const char * argv[])
{
  ofstream outfile;
  outfile.open("word5l.txt");
  ifstream file;
  file.open("words5.txt");

  string line;
  while(getline(file, line))
    {
      for(int i = 0; i < sizeof(line); i++){
	line[i] = tolower(line[i]);
      }
      outfile << line << '\n';
    }
  file.close();
  outfile.close();
  return 0;
}
  
