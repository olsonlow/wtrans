# README #

This program is intended to collect data about the transformability of words.  i.e. whether a word is transformable to another word.

There are two ways to run the program.  

1.  To test the transformability of a specific word to a noter specific word do the following.
a. open the morph.cpp file in an editor.  Replace the word "words3" "words4" or "words5" in two locations where the program opens and reads in the dictionary file whit "words" which is a dictionary with words of all lengths.

2. close the lie and type make to create the target Trans.out

3.  Type ./Trans.out follows by the two words you want to test... ./Trans.out ace cat

4.  This will run the program for only those words.

The second way is as follows.

1. modify the morph.cpp file by replacing the filename of the "words" dictionary with one of the numbered filenames like "words3" for 3 letter words.

2.  do a make then do make Run.out this will create the correct targets.

3.  type ./Ron.out into your terminal.  This will kick off a very lengthy process of attempting to transform every word in the 3, 4 or 5 letter word dictionary you selected against all the other words in the dictionary.

WARNING the method described above will take a very long time to run.  if you wish to collect the data in a file you should redirect the output on the command line by typing the command like this.  ./Run.out >> filename.txt

This will allow the data to be collected.

The program still needs to be modified to collect usable statistical data for use in graphing the transformability coefficient or all the words in each of the three dictionaries.